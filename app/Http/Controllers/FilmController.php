<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Film;
use App\Models\Genre;
use App\Models\Review;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.create',compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'genre_id' => 'required'
        ]);

        $fileName = time() . '.' . $request->poster->extension();
        $request->poster->move(public_path('storage/film'), $fileName);
        
        $film = new Film;
        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->poster = $fileName;
        $film->genre_id = $request->input('genre_id');
        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        return view('film.edit', compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Film $film)
    {
        $fileName = '';

        if ($request->hasFile('poster')) {
          $fileName = time() . '.' . $request->poster->extension();
          $request->poster->move(public_path('storage/film'), $fileName);
          if ($film->poster) {
            unlink('storage/film/'.$film->poster);
          }
        } else {
          $fileName = $film->poster;
        }

        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->poster = $fileName;
        $film->genre_id = $request->input('genre_id');
        $film->save();

        return redirect()->route('film.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Film $film)
    {
        if($film->poster){
            unlink('storage/film/'.$film->poster);
        }
        $film->delete();

        return redirect()->route('film.index')->with([
            'message' => 'film deleted successfully!', 
            'status' => 'success'
        ]);
    }

    public function tampil()
    {
        $film = Film::all();
        return view('film.tampil', compact('film'));
    }

    public function review($id)
    {
        $film = Film::find($id);
        return view('film.review', compact('film'));
    }
}
