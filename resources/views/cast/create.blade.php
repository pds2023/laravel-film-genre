@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Cast
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama </label>
        <input type="text" class="form-control" name="nama" id="nama">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
        <label for="umur">Umur </label>
        <input type="number" id="umur" class="form-control" name="umur" min=17 max=100><br>
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <label for="bio">Bio</label><br>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="5"></textarea><br><br>
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection