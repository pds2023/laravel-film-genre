@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Create Film
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">

        <label for="judul">Judul </label>
        <input type="text" class="form-control" name="judul" id="judul">
        @error('judul')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <label for="ringkasan">Ringkasan</label><br>
        <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="5"></textarea><br><br>
        @error('ringkasan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <label for="tahun">Tahun </label>
        <input type="number" id="tahun" class="form-control" name="tahun"><br>
        @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <label class="col-sm-3 col-form-label">Poster</label>
        <div class="col-sm-9">
            <input type="file" class="form-control" name="poster" @error('poster') is-invalid @enderror>
        </div>
        @error('poster')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Genre</label>
            </div>
            <select id="genre_id" name="genre_id" class="custom-select" id="inputGroupSelect01">
              <option selected>Choose...</option>
                @forelse ($genre as $key=>$value)
                    <option value="{{ $value->id }}">{{ $value->nama }}</option>
                @empty
                    <option value="">No Genre List</option>
                @endforelse
            </select>
          </div>
        @error('poster')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection