@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Edit Film
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">

        <label for="judul">Judul </label>
        <input type="text" class="form-control" name="judul" id="judul" value="{{$film->judul}}"><br>
        @error('judul')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <label for="ringkasan">Ringkasan</label>
        <textarea class="form-control" name="ringkasan" id="ringkasan" cols="30" rows="5">{{$film->ringkasan}}</textarea><br>
        @error('ringkasan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <label for="tahun">Tahun </label>
        <input type="number" id="tahun" class="form-control" name="tahun" value="{{$film->tahun}}"><br>
        @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror

        <label  for="poster">Poster</label>
        <input type="file" class="form-control" name="poster"><br>
        @error('poster')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Genre</label>
            </div>
            <select id="genre_id" name="genre_id" class="custom-select" id="inputGroupSelect01">
                <option value={{$film->genre_id}} selected>{{$film->genre->nama}}</option>
                @forelse ($genre as $key=>$value)
                    <option value="{{ $value->id }}">{{ $value->nama }}</option>
                @empty
                    <option value="">No Genre List</option>
                @endforelse
            </select>
          </div>
        @error('poster')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

    </div>

    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection