@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Film
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<a href="{{ route('film.create') }}" class="btn btn-primary mb-3">+Tambah</a>
<table id="example1" class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">#</th>
        <th scope="col">judul</th>
        <th scope="col">ringkasan</th>
        <th scope="col">tahun</th>
        <th scope="col">poster</th>
        <th scope="col">genre</th>
        <th scope="col">aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($film as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->judul}}</td>
                <td>{{$value->ringkasan}}</td>
                <td>{{$value->tahun}}</td>
                <td><img src="{{asset('storage/film/'. $value->poster)}}" style="max-width:100%;height: 100px;" class="img-fluid" alt="{{$value->poster}}"></td>
                <td>{{$value->genre->nama}}</td>
                <td>
                <a href="{{ route('film.edit',$value->id) }}" class="btn btn-success btn-xs py-0">Edit</a>
                <a href="{{ route('film.show',$value->id) }}" class="btn btn-secondary btn-xs py-0">Detail</a>
                <form action="/film/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger btn-xs py-0" value="Delete">
                </form>
                    
                </td>
            </tr>
        @empty
            <tr colspan=7>
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection