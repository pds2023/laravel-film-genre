@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Film
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
<style>
    .card-img-top {
    width: 100%;
    height: 25vw;
    object-fit: cover;
}
</style>
@endpush

@section('content')
<div class="card-deck">
    <div class="card">
        <img class="card-img-top" src="{{asset('storage/film/'. $film->poster)}}" alt="{{$film->judul}}">
        <div class="card-body">
            <h5 class="card-title">{{$film->judul}}</h5>
            <p class="card-text">{{$film->ringkasan}}</p>
            <p class="card-text">{{$film->genre->nama}}</p>
        </div>
    </div>     
</div>


<div class="card">
    <div class="card-body">
        <h4>Display Review</h4><hr />
        @forelse ($film->reviews as $review)
            <img src="{{asset('storage/user/'. $review->users->photo)}}" style="height: 30px;width:30px;" class="img-circle elevation-2" alt="User Image">
            <strong>{{ $review->users->name }}</strong>
            <p>{{ $review->content }}</p>
            <hr />
        @empty
            Tidak ada review
        @endforelse
            
        @auth
        <h4>Add Review</h4>
            <form method="post" action="{{ route('review.store') }}">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" name="content"></textarea>
                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}" />
                    <input type="hidden" name="film_id" value="{{ $film->id }}" />
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Add Review" />
                </div>
            </form>
            @endauth

            @guest
                Login untuk membuat review
            @endguest
        
    </div>
</div>

@endsection