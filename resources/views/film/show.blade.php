@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Film Detail
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')


<div class="card" style="width: 25rem;">
    <img src="{{asset('storage/film/'. $film->poster)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{$film->judul}}</h5>
      <p class="card-text">{{$film->ringkasan}}</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Tahun : {{ $film->tahun}}</li>
      <li class="list-group-item">Genre : {{ $film->genre->nama}}</li>
    </ul>
  </div>

    {{-- @forelse ($film->review as $key=>$value)
    <div class="media">
    <img src="{{asset('img/user-placeholder.png')}}" style="max-width:100%;height: 50px;" class="img-fluid img-circle elevation-2" alt="...">
    <div class="media-body">
      <h5 class="mt-0">{{$value->users->nama}}</h5>
      <p>{{$value->content}}</p>
    </div>
    @empty
    <tr colspan=6>
        <td>Belum ada data review</td>
    </tr>  
    @endforelse  
  
  </div> --}}

@endsection