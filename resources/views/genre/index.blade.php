@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Genre
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<a href="{{ route('genre.create') }}" class="btn btn-primary mb-3">+Tambah</a>
<table id="example1" class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>
                <a href="{{ route('genre.edit',$value->id) }}" class="btn btn-success btn-xs py-0">Edit</a>
                <a href="{{ route('genre.show',$value->id) }}" class="btn btn-secondary btn-xs py-0">Detail</a>
                <form action="/genre/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger btn-xs py-0" value="Delete">
                </form>
                    
                </td>
            </tr>
        @empty
            <tr colspan=6>
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection