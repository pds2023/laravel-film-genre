@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Film berdasarkan genre 
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

@forelse ($genre->films as $key=>$value)

<div class="card" style="width: 25rem;">
    <img src="{{asset('storage/film/'. $value->poster)}}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">{{$value->judul}}</h5>
      <p class="card-text">{{$value->ringkasan}}</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Tahun : {{ $value->tahun}}</li>
      <li class="list-group-item">Genre : {{ $genre->nama}}</li>
    </ul>
  </div>

@empty
  <tr colspan=6>
      <td>No data</td>
  </tr>  
@endforelse  

@endsection