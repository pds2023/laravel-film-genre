<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        @yield('user-foto')
      </div>
      <div class="info">
        @auth
        <a href="/user/{{auth()->user()->id}}" class="d-block">@yield('user')</a>
        @endauth

        @guest
        <a href="/login" class="d-block">@yield('user')</a>
        @endguest
        
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        @auth
        <li class="nav-item">
          <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard<i class="right fas fa-angle-left"></i></p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="/user" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>User</p>
                </a>
            </li>
            <li class="nav-item">
              <a href="/cast" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cast</p>
              </a>
          </li>
          <li class="nav-item">
            <a href="/genre" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Genre</p>
            </a>
        </li>
        <li class="nav-item">
          <a href="/film" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Film</p>
          </a>
        </li>
        </ul>
        </li>
        
        @endauth
        
        <li class="nav-item">    
            <a href="#" class="nav-link">   
                <i class="nav-icon fas fa-th"></i>
                <p>Menu<i class="right fas fa-angle-left"></i></p>
            </a>
            <ul class="nav nav-treeview">
  
              
            <li class="nav-item">
              <a href="/genre" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Genre</p>
              </a>
          </li>
          <li class="nav-item">
            <a href="/film" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Film</p>
            </a>
          </li>
          </ul>
        </li>
        @auth
          <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link" 
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Logout</p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
        @endauth
        
        
        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>