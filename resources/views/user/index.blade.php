@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Home
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<a href="{{ route('user.create') }}" class="btn btn-primary mb-3">+Tambah</a>
<table id="example1" class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">email</th>
        <th scope="col">password</th>
        <th scope="col">photo</th>
        <th scope="col">aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($users as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->name}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->password}}</td>
                <td>
                    @if($value->photo)
                        <img src="{{ asset('storage/user/'.$value->photo) }}" style="height: 50px;width:50px;" class="img-circle elevation-2" alt="User Image">
                    @else 
                        <img src="{{asset('img/user-placeholder.png')}}" style="height: 50px;width:50px;" class="img-circle elevation-2" alt="User Image">
                    @endif
                </td>
                <td>
                <a href="{{ route('user.edit',$value->id) }}" class="btn btn-success btn-xs py-0">Edit</a>
                <a href="{{ route('user.show',$value->id) }}" class="btn btn-secondary btn-xs py-0">Detail</a>
                <a href="{{ route('user.destroy',$value->id) }}" class="btn btn-danger btn-xs py-0"
                    onclick="event.preventDefault();
                    document.getElementById('delete-form').submit();">Delete</a>

                <form id="delete-form" method="POST" action="{{ route('user.destroy',$value->id) }}">
                    @csrf
                    @method('delete')
                </form>
                    
                </td>
            </tr>
        @empty
            <tr colspan=6>
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection