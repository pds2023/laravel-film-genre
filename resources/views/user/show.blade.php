@extends('layouts.master')

@section('sitetitle','MyMovieApp')

@section('title')
Film
@endsection

@section('user')
    @auth
    {{ ucwords(auth()->user()->name) }}
    @endauth

    @guest
    Guest
    @endguest
@endsection

@section('user-foto')
    @auth
        @if(auth()->user()->photo)
            <img src="{{asset('storage/user/'. auth()->user()->photo)}}" class="img-circle elevation-2" alt="User Image">
        @else 
            <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
        @endif
    
    @endauth

    @guest
        <img src="{{asset('img/user-placeholder.png')}}" class="img-circle elevation-2" alt="User Image">
    @endguest

@endsection

@push('scripts')
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
    <div class="card" style="width: 18rem;">
    <img class="card-img-top" src="{{ asset('storage/user/'.$user->photo) }}" alt={{$user->name}}>
    <div class="card-body">
      <h5 class="card-title">{{$user->name}}</h5>
      <p class="card-text">{{$user->profile->bio}}</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item">Alamat : {{$user->profile->alamat}}</li>
      <li class="list-group-item">Umur : {{$user->profile->umur}}</li>
      <li class="list-group-item">Email : {{$user->email}}</li>
    </ul>
    <div class="card-body">
      <a href="../profile/{{$user->profile->id}}/edit" class="card-link">Edit Profile</a>
    </div>
  </div>
@endsection