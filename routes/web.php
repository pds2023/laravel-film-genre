<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\ReviewController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('film/{id}/review', [FilmController::class,'review']);
Route::get('film/tampil', [FilmController::class,'tampil']);

Route::group(['middleware' => 'auth'], function () {
    //Route User
    Route::resource('user',UserController::class,['except' => ['index', 'show']]);

    //Route Profile
    Route::resource('profile',ProfileController::class,['except' => ['index', 'show']]);

    //Route Genre
    Route::resource('genre',GenreController::class,['except' => ['index', 'show']]);

    //Route Cast
    Route::resource('cast',CastController::class,['except' => ['index', 'show']]);

    //Route Film
    Route::resource('film',FilmController::class,['except' => ['index', 'show']]);

    //Route Review
    Route::resource('review',ReviewController::class, ['except' => ['index', 'show']]);
});

//Route User
Route::resource('user',UserController::class, ['only' => ['index', 'show']]);

//Route Profile
Route::resource('profile',ProfileController::class, ['only' => ['index', 'show']]);

//Route Genre
Route::resource('genre',GenreController::class, ['only' => ['index', 'show']]);

//Route Cast
Route::resource('cast',CastController::class, ['only' => ['index', 'show']]);

//Route Film
Route::resource('film',FilmController::class, ['only' => ['index', 'show']]);

//Route Review
Route::resource('review',ReviewController::class, ['only' => ['index', 'show']]);


